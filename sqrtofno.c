#include <stdio.h>
#include <math.h>

int main()
{
    int n;
    float r;
    printf("Enter the number:");
    scanf("%d",&n);
    r=sqrt(n);
    printf("The sum of square root of %d is %f.\n",n,r);
    return 0;
}    
