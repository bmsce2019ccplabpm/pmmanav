#include <stdio.h>

int main()
{
    struct DOB
    {
        int day;
        int month;
        int year;
    };

    struct data
    {
        char name[20];
        int roll_no;
        struct DOB date;
    };

    struct data std;
    printf("Enter the name of student:");
    gets(std.name);
    printf("Enter roll number:");
    scanf("%d",&std.roll_no);
    printf("Enter DOB:");
    scanf("%d %d %d",&std.date.day,&std.date.month,&std.date.year);
    printf("*******STUDENT DETAIL'S*******");
    printf("\nNAME:");
    puts(std.name);
    printf("ROLL NUMBER:%d",std.roll_no);
    printf("\nDOB:%d %d %d",std.date.day,std.date.month,std.date.year);
    return 0;
}